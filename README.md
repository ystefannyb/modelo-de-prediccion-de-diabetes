**MODELO DE PREDICCIÓN DE DIABETES**

![Banner](./Imagenes/BannerIA.png)

[Video](https://youtu.be/X4x9gGsE3uM)



_Autores:_ Yuly Stefanny Bohórquez Bohórquez, Cristián Alexander Malagón y Cristián David Tafur Ocampo

_Objetivo:_ El objetivo principal de este proyecto es crear un modelo de predicción de diabetes preciso y confiable utilizando inteligencia artificial, lo que puede tener un impacto significativo en la detección temprana de la enfermedad, permitiendo así una intervención a tiempo y un manejo más efectivo de los pacientes afectados. 

_Dataset:_ Este dataset es el resultado de la unión de dos dataset que fueron importantes en la realización del proyecto. Además, al dataset se le realiza un aumento de datos, mediante distribuciones de cada columna, y de características, mediante regresores; logrando así balancear los datos y obtener mejores resultados.
El dataset cuenta con **653680 filas** y **9 columnas**.
https://drive.google.com/file/d/1QzLcRAsjKj5wiArYQm3rtOcVvYwdhcbQ/view?usp=drive_link

_Modelos:_ GaussianNB, DesicionTree Classifier, RandomForest Classifier, SVC y DeepLearning 

_Enlaces:_https://gitlab.com/ystefannyb/modelo-de-prediccion-de-diabetes/-/tree/main
https://drive.google.com/drive/folders/1uBxRcF9SdmD-xByuM_HmpgXAtpnNXEoX

notebooks: https://drive.google.com/drive/folders/1uBxRcF9SdmD-xByuM_HmpgXAtpnNXEoX?usp=drive_link


